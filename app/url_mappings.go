package app

import (
	"gitlab.com/guyamuff/bookstore_users-api/controllers/ping"
	"gitlab.com/guyamuff/bookstore_users-api/controllers/users"
)

func mapUrls() {
	router.GET("/ping", ping.Ping)

	router.POST("/users", users.CreateUser)
	router.GET("/users/:userid", users.GetUser)
	router.PUT("/users/:userid", users.UpdateUser)
	router.PATCH("/users/:userid", users.UpdateUser)
	router.DELETE("/users/:userid", users.Delete)
	router.GET("/internal/users/search", users.SearchByStatus)
	router.POST("/users/login", users.Login)
}
