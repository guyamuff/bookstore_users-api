package app

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

var (
	router = gin.Default()
)

func StartApplication() {
	mapUrls()
	err := router.SetTrustedProxies(nil)
	if err != nil {
		fmt.Print(err.Error())
		return
	}
	err = router.Run(":8080")
	if err != nil {
		fmt.Print(err.Error())
		return
	}
}
