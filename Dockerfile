# syntax=docker/dockerfile:1

FROM golang:1.18-alpine 
WORKDIR /main
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
COPY app/ /main/app/
COPY controllers/ /main/controllers/
COPY datasources/ /main/datasources/
COPY domain/ /main/domain/
COPY services/ /main/services/
COPY utils/ /main/utils/

RUN go build -o /bookstore_users-api .
EXPOSE 8080

CMD [ "/bookstore_users-api" ]


