package users

import (
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
)

const ActiveStatus = "active"

type User struct {
	Id          int64  `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	DateCreated string `json:"date_created"`
	Status      string `json:"status"`
	Password    string `json:"password"`
}

func (user *User) Validate() *errors.RestErr {
	if user.Email == "" {
		return errors.NewBadRequestError("Invalid email " + user.Email)
	}
	if user.Password == "" {
		return errors.NewBadRequestError("Must specify password")
	}

	return nil
}

type UserLoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
