package users

import (
	"database/sql"
	"fmt"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
	"gitlab.com/guyamuff/bookstore_users-api/datasources/mysql/users_db"
)

const (
	insertQuery = "INSERT INTO user(first_name, last_name, email, date_created, status,  password) VALUES (?, ?, ?, ?, ?, ?)"
	selectQuery = "SELECT first_name, last_name, email, date_created, status FROM user WHERE id=?;"
	updateQuery = "UPDATE user SET first_name = ?, last_name = ?, email = ? WHERE id = ?;"
	deleteQuery = "DELETE FROM user WHERE id = ?;"
	searchQuery = "SELECT id, first_name, last_name, email, date_created, status FROM user WHERE status = ?;"
	loginQuery  = "SELECT id, first_name, last_name, email FROM user WHERE email = ? and password = ?;"
)

func (user *User) Save() *errors.RestErr {

	stmt, err := users_db.Client.Prepare(insertQuery)
	if err != nil {
		return errors.NewInternalError(err.Error())
	}
	defer stmt.Close()

	res, err := stmt.Exec(user.FirstName, user.LastName, user.Email, user.DateCreated, user.Status, user.Password)
	if err != nil {

		if users_db.IsUniqueConstraintViolation(err) {
			return errors.NewBadRequestError(err.Error())
		}

		return errors.NewInternalError(err.Error())
	}

	userId, err := res.LastInsertId()
	if err != nil {
		return errors.NewInternalError(err.Error())
	}

	user.Id = userId
	return nil
}

func (user *User) Get() *errors.RestErr {

	stmt, err := users_db.Client.Prepare(selectQuery)
	if err != nil {
		return errors.NewInternalError(err.Error())
	}
	defer stmt.Close()
	row := stmt.QueryRow(user.Id)
	err = row.Scan(&user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status)
	if err != nil {
		if sql.ErrNoRows == err {
			return errors.NewNotFoundError(fmt.Sprintf("User Id %d not found", user.Id))
		}
		return errors.NewInternalError(err.Error())
	}

	return nil
}

func (user *User) Login() *errors.RestErr {

	stmt, err := users_db.Client.Prepare(loginQuery)
	if err != nil {
		return errors.NewInternalError(err.Error())
	}
	defer stmt.Close()
	row := stmt.QueryRow(user.Email, user.Password)
	err = row.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email)
	if err != nil {
		if sql.ErrNoRows == err {
			return errors.NewNotFoundError(fmt.Sprintf("User email %s and password %s not found", user.Email, user.Password))
		}
		return errors.NewInternalError(err.Error())
	}

	return nil
}

func (user *User) Update() *errors.RestErr {

	stmt, err := users_db.Client.Prepare(updateQuery)
	if err != nil {
		return errors.NewInternalError(err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(user.FirstName, user.LastName, user.Email, user.Id)
	if err != nil {

		if users_db.IsUniqueConstraintViolation(err) {
			return errors.NewBadRequestError(err.Error())
		}

		return errors.NewInternalError(err.Error())
	}
	return nil
}

func (user *User) Delete() *errors.RestErr {

	stmt, err := users_db.Client.Prepare(deleteQuery)
	if err != nil {
		return errors.NewInternalError(err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(user.Id)
	if err != nil {
		return errors.NewInternalError(err.Error())
	}
	return nil
}

func Search(status string) ([]User, *errors.RestErr) {
	stmt, err := users_db.Client.Prepare(searchQuery)
	if err != nil {
		return nil, errors.NewInternalError(err.Error())
	}
	defer stmt.Close()

	rows, sqlErr := stmt.Query(status)
	if sqlErr != nil {
		return nil, errors.NewInternalError(sqlErr.Error())
	}
	defer rows.Close()

	ret := make([]User, 0, 0)
	for rows.Next() {
		var user User
		err = rows.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status)
		if err != nil {
			return nil, errors.NewInternalError(err.Error())
		}
		ret = append(ret, user)
	}

	if len(ret) == 0 {
		return nil, errors.NewNotFoundError(fmt.Sprintf("No users with status %s found", status))
	}

	return ret, nil
}

/*
CREATE TABLE `users_db`.`user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `date_created` DATETIME NULL,
  `status` VARCHAR(45) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE);
*/
