package users

import "encoding/json"

type PublicUser struct {
	Id          int64  `json:"id"`
	DateCreated string `json:"date_created"`
	Status      string `json:"status"`
}

type PrivateUser struct {
	Id          int64  `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	DateCreated string `json:"date_created"`
	Status      string `json:"status"`
}

func (user *User) Marshall(isPrivate bool) interface{} {
	if isPrivate {
		userJson, _ := json.Marshal(user)
		var privateUser PrivateUser
		_ = json.Unmarshal(userJson, &privateUser)
		return privateUser
	}
	return PublicUser{
		Id:          user.Id,
		DateCreated: user.DateCreated,
		Status:      user.Status,
	}
}
