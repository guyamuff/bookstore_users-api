package services

import (
	"crypto/sha256"
	"fmt"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
	"gitlab.com/guyamuff/bookstore_users-api/domain/users"
	"gitlab.com/guyamuff/bookstore_users-api/utils/date_utils"
)

type userServiceInterface interface {
	CreateUser(user users.User) (*users.User, *errors.RestErr)
	GetUser(userId int64) (*users.User, *errors.RestErr)
	Login(string, string) (*users.User, *errors.RestErr)
	UpdateUser(user users.User, patch bool) (*users.User, *errors.RestErr)
	DeleteUser(userId int64) *errors.RestErr
	SearchByStatus(status string) ([]users.User, *errors.RestErr)
}

type userService struct {
}

var UserService userServiceInterface = &userService{}

func (s *userService) CreateUser(user users.User) (*users.User, *errors.RestErr) {

	if restErr := user.Validate(); restErr != nil {
		return nil, restErr
	}

	user.DateCreated = date_utils.GetNowString()
	user.Status = users.ActiveStatus
	user.Password = hashPass(user.Password)
	return &user, user.Save()
}

func hashPass(pass string) string {
	hash1 := sha256.Sum256([]byte(pass))
	return fmt.Sprintf("%x", hash1)
}

func (s *userService) GetUser(userId int64) (*users.User, *errors.RestErr) {
	ret := users.User{
		Id: userId,
	}
	restErr := ret.Get()
	return &ret, restErr
}

func (s *userService) Login(email string, password string) (*users.User, *errors.RestErr) {
	ret := users.User{
		Email:    email,
		Password: password,
	}
	ret.Password = hashPass(ret.Password)
	restErr := ret.Login()
	return &ret, restErr
}

func (s *userService) UpdateUser(user users.User, patch bool) (*users.User, *errors.RestErr) {

	dbUser, err := s.GetUser(user.Id)
	if err != nil {
		return nil, err
	}

	if patch {
		if "" != user.FirstName {
			dbUser.FirstName = user.FirstName
		}
		if "" != user.LastName {
			dbUser.LastName = user.LastName
		}
		if "" != user.Email {
			dbUser.Email = user.Email
		}
	} else {
		dbUser.FirstName = user.FirstName
		dbUser.LastName = user.LastName
		dbUser.Email = user.Email
	}

	return dbUser, dbUser.Update()
}

func (s *userService) DeleteUser(userId int64) *errors.RestErr {
	dbUser, err := s.GetUser(userId)
	if err != nil {
		return err
	}

	return dbUser.Delete()
}

func (s *userService) SearchByStatus(status string) ([]users.User, *errors.RestErr) {
	return users.Search(status)
}
