package users_db

import (
	"database/sql"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/logger"
	"os"
)

var (
	Client *sql.DB

	user = os.Getenv("go.mysql.user")
	pass = os.Getenv("go.mysql.pass")
	host = os.Getenv("go.mysql.host")
	port = os.Getenv("go.mysql.port")
	db   = os.Getenv("go.mysql.db")

	UniqueErrorCode = mysql.MySQLError{
		Number: 1062,
	}
)

func init() {
	var err error

	err = mysql.SetLogger(logger.Log)
	if err != nil {
		panic(err)
	}
	connectStr := fmt.Sprintf("%s:%s@(%s:%s)/%s", user, pass, host, port, db)
	Client, err = sql.Open("mysql", connectStr)

	if err != nil {
		panic(err)
	}

	err = Client.Ping()
	if err != nil {
		panic(err)
	}

	logger.Log.Info("Initialized MySql DB")
}

func IsUniqueConstraintViolation(err error) bool {
	ret := false
	mysqlErr, ok := err.(*mysql.MySQLError)
	if ok {
		ret = UniqueErrorCode.Number == mysqlErr.Number
	}
	return ret // Trying to keep all mysql specific logic in here. Could use an interface to be used
	// in the dao. Not sure if it is worth it at this time.
}
