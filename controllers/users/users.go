package users

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/oauth"
	"gitlab.com/guyamuff/bookstore_users-api/domain/users"
	"gitlab.com/guyamuff/bookstore_users-api/services"
	"net/http"
	"strconv"
)

func CreateUser(c *gin.Context) {
	var user users.User
	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := errors.NewBadRequestError("Invalid JSON body")
		c.JSON(restErr.Status, restErr)
		return
	}

	result, saveErr := services.UserService.CreateUser(user)
	if saveErr != nil {
		c.JSON(saveErr.Status, saveErr)
		return
	}
	c.JSON(http.StatusCreated, result)
}

func GetUser(c *gin.Context) {
	userId, parseErr := parseUserId(c)
	if parseErr != nil {
		c.JSON(parseErr.Status, parseErr)
		return
	}

	o := oauth.NewOAuth()
	token, err := o.GetAuth(c.Request)
	if err != nil {
		c.JSON(err.Status, err)
		return
	}

	if token == nil {
		authErr := errors.NewAuthError()
		c.JSON(authErr.Status, authErr)
		return
	}

	ret, findErr := services.UserService.GetUser(userId)
	if findErr != nil {
		c.JSON(findErr.Status, findErr)
		return
	}

	c.JSON(http.StatusOK, ret.Marshall(o.IsPrivate(c.Request) || ret.Id == token.UserId))
	return
}

func Login(c *gin.Context) {
	var request = users.UserLoginRequest{}
	err := c.ShouldBindJSON(&request)
	if err != nil {
		restErr := errors.NewBadRequestError("Bad request")
		c.JSON(restErr.Status, restErr)
		return
	}

	ret, newErr := services.UserService.Login(request.Email, request.Password)
	if newErr != nil {
		c.JSON(newErr.Status, newErr)
		return
	}

	c.JSON(http.StatusOK, ret.Marshall(true))
	return
}

func UpdateUser(c *gin.Context) {
	userId, parseErr := parseUserId(c)
	if parseErr != nil {
		c.JSON(parseErr.Status, parseErr)
		return
	}

	var user users.User
	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := errors.NewBadRequestError("Invalid JSON body")
		c.JSON(restErr.Status, restErr)
		return
	}
	user.Id = userId

	patch := http.MethodPatch == c.Request.Method

	ret, findErr := services.UserService.UpdateUser(user, patch)
	if findErr != nil {
		c.JSON(findErr.Status, findErr)
		return
	}

	c.JSON(http.StatusOK, ret)
	return
}

func Delete(c *gin.Context) {
	userId, restErr := parseUserId(c)
	if restErr != nil {
		c.JSON(restErr.Status, restErr)
		return
	}

	restErr = services.UserService.DeleteUser(userId)
	if restErr != nil {
		c.JSON(restErr.Status, restErr)
		return
	}

	c.Status(http.StatusNoContent)
	return
}

func SearchByStatus(c *gin.Context) {
	status := c.Query("status")
	if "" == status {
		// Request parameter "status" required
		restErr := errors.NewBadRequestError("Request parameter 'status' required")
		c.JSON(restErr.Status, restErr)
		return
	}

	res, restErr := services.UserService.SearchByStatus(status)
	if restErr != nil {
		c.JSON(restErr.Status, restErr)
		return
	}

	ret := make([]interface{}, len(res))
	isPrivate := c.GetHeader("X-Private") == "true"
	for i, user := range res {
		ret[i] = user.Marshall(isPrivate)
	}
	c.JSON(http.StatusOK, ret)
}

func parseUserId(c *gin.Context) (int64, *errors.RestErr) {
	idStr := c.Param("userid")
	userId, parseErr := strconv.ParseInt(idStr, 10, 64)
	if parseErr != nil {
		return 0, errors.NewBadRequestError("Can't parse userId " + idStr)
	}
	return userId, nil
}
